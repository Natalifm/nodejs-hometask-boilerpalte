const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', async (req, res, next) => {
  // TODO: Implement route controllers for user
  try {
    res.data = await UserService.getAllUsers();
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware);

router.get('/:id', async (req, res, next) => {
  try {
    res.data = await UserService.getOneUser({id: req.params.id});
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.post('/', createUserValid, async (req, res, next) => {
  try {
    res.data = await UserService.createUser(req.body)
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.put('/:id', updateUserValid, async (req, res, next) => {
  try {
    res.data = await UserService.updateUser(req.params.id, req.body);
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

router.delete('/:id', async (req, res, next) => {
  try {
    res.data = await UserService.deleteUser(req.params.id)
  } catch (error) {
    res.err = error;
  } finally {
    next();
  }
}, responseMiddleware)

module.exports = router;