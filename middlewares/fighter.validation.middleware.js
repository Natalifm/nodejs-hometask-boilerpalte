const {fighter} = require('../models/fighter');
const {check, validationResult} = require('express-validator');

const createFighterValid = async (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    await check('name', 'No name entered')
        .notEmpty().run(req);
    await check('power', 'Power should be in the range from 0 to 100')
        .isInt({min: 1, max: 100}).run(req);
    await check('defense', 'Defence should be in the range from 1 to 10')
        .isInt({min: 1, max: 10}).run(req);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        let errMessage = '';
        errors.errors.forEach(msg => errMessage += `${msg.msg}; `);
        return res.status(400)
            .json({
                error: true,
                message: errMessage
            });
    }
    return next();
}

const updateFighterValid = async (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    for (let key in req.body) {
        if (key === 'name') {
            await check('name', 'No name entered')
                .notEmpty().run(req);
            continue;
        } else if (key === 'power') {
            await check('power', 'Power should be in the range from 1 to 100')
                .isInt({min: 1, max: 100}).run(req);
            continue;
        } else if (key === 'defense') {
            await check('defense', 'Defence should be on the range from 1 to 10')
                .isInt({min: 1, max: 10}).run(req);
            continue;
        } else {
            return res.status(400)
                .json({
                    error: true,
                    message: 'Not updated'
                });
        }
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        let errMessage = '';
        errors.errors.forEach(msg => errMessage += `${msg.msg}; `);
        return res.status(400)
            .json({
                error: true,
                message: errMessage
            });
    }
    return next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;