const {user} = require('../models/user');
const {check, validationResult} = require('express-validator');


const createUserValid = async (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    await check('email', 'Enter an email in a format as @gmail.com')
        .matches(/[a-zA-Z0-9]+(@gmail.com$)/).run(req);
    await check('firstName', 'Enter a first name')
        .notEmpty().run(req);
    await check('lastName', 'Enter a last name')
        .notEmpty().run(req);
    await check('password', 'The password must be 3+ chars long and Do not use a common word as the password')
        .not().isIn(['123', 'password', 'god'])
        .isLength({min: 3})
        .run(req);
    await check('phoneNumber', 'Enter a phoneNumber in format +380')
        .isMobilePhone(['uk-UA'], {strictMode: true})
        .run(req);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        let errMessage = '';
        errors.errors.forEach(msg => errMessage += `${msg.msg}; `);
        return res.status(400)
            .json({
                error: true,
                message: errMessage
            });
    }
    return next();
}

const updateUserValid = async (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    for (let key in req.body) {
        if (key === 'email') {
            await check('email', 'Enter an email in a format as @gmail.com')
                .matches(/[a-zA-Z0-9]+(@gmail.com$)/)
                .run(req);
            continue;
        } else if (key === 'password') {
            await check('password', 'The password must be 3+ chars long')
                .not().isIn(['123', 'password', 'god'])
                .isLength({min: 3})
                .run(req);
            continue;
        } else if (key === 'firstName') {
            await check(key, 'Enter a first name')
                .notEmpty().run(req);
            continue;
        } else if (key === 'lastName') {
            await check(key, 'Enter a last name')
                .notEmpty().run(req)
        } else if (key === 'phoneNumber') {
            await check('phoneNumber', 'Enter a phoneNumber in format +380')
                .isMobilePhone(['uk-UA'], {strictMode: true})
                .run(req);
            continue;
        } else {
            return res.status(400)
                .json({
                    error: true,
                    message: 'Not updated'
                });
        }
    }
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        let errMessage = '';
        errors.errors.forEach(msg => errMessage += `${msg.msg}; `);
        return res.status(400)
            .json({
                error: true,
                message: errMessage
            });
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;