const { FightRepository } = require('../repositories/fightRepository');

class FightService {
    // OPTIONAL TODO: Implement methods to work with fights
    getAllFight() {
        const item = FightRepository.getAll();
        if(!item.length) {
            throw Error('No fight in datadase')
        }
        return item;
    }
    getOne(id) {
        const item = FightRepository.getOne({id});
        if(!item) {
            throw Error('Fight not found')
        }
        return item;
    }
    create(data) {
        const item = FightRepository.create(data);
        if(!item) {
            throw Error('No fight')
        }
        return item;
    }
    delete(id) {
        const item = FightRepository.delete(id);
        if (!item) {
            throw Error('Can not delete')
        }
        return item;
    }
}

module.exports = new FightService();