const {FighterRepository} = require('../repositories/fighterRepository');
const {fighter} = require('../models/fighter');

class FighterService {
    // TODO: Implement methods to work with fighters

    getAllFighter() {
        const fighters = FighterRepository.getAll()
        if (!fighters) {
            throw Error('No fighters in database')
        }
        return fighters
    }

    getOneFighter(id) {
        const fighter = FighterRepository.getOne({id});
        if (!fighter) {
            throw Error('No fighter with this id');
        }
        return fighter;
    }

    createFighter(data) {
        const newFighter = {...fighter, ...data}
        const item = FighterRepository.create(newFighter)
        if (!item) {
            throw Error('No fighter created')
        }
        return item
    }

    updateFighter(id, user) {
        const fighterId = FighterRepository.getOne({id});
        if (!fighterId) {
            throw Error('Fighter not found');
        }
        let fieldToUpdate = {}
        for (let param in fighter) {
            !!fighter[param] && (fieldToUpdate[param] = fighter[param])
        }
        const updatedFighter = FighterRepository.update(id, fieldToUpdate);
        if (!updatedFighter) {
            throw Error('Can not update fighter');
        }
        return updatedFighter;
    }

    deleteFighter(id) {
        const deletedFighter = FighterRepository.delete(id);
        if (!deletedFighter.length) {
            throw Error('No fighter with this id');
        }
        return deletedFighter;
    }
}


module.exports = new FighterService();