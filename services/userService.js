const {UserRepository} = require('../repositories/userRepository');

class UserService {
    // TODO: Implement methods to work with user
    createUser(data) {
        const user = UserRepository.getOne({
            email: data.email
        })
        if (!user) {
            const userCreated = UserRepository.create(data)
            if (!userCreated) {
                throw Error(`User wost't created`)
            }
            return userCreated
        } else {
            throw Error('This email is used')
        }
    }

    getAllUsers() {
        const users = UserRepository.getAll();
        if (!users) {
            throw Error('No users found');
        }
        return users;
    }

    getOneUser(search) {
        const user = UserRepository.getOne(search);
        if (!user) {
            throw Error(`No user found`);
        }
        return user;
    }

    updateUser(id, user) {
        const userId = UserRepository.getOne({id});
        if (!userId) {
            throw Error('User not found');
        }
        let fieldToUpdate = {}
        for (let param in user) {
            !!user[param] && (fieldToUpdate[param] = user[param])
        }
        const updatedUser = UserRepository.update(id, fieldToUpdate);
        if (!updatedUser) {
            throw Error('Can not update user');
        }
        return updatedUser;
    }

    deleteUser(id) {
        const deletedUser = UserRepository.delete(id);
        if (!deletedUser.length) {
            throw Error(`No user with the id of ${id}`);
        }
        return deletedUser;
    }
}

module.exports = new UserService();